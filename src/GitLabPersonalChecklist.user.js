import { createScratchpad } from "./gl/create.js";

import { attachRTE } from "./common/RTE.js";
import { loadLines } from "./common/DB.js";

import { scratchpadMouse } from "./observation/mouse.js";
import { editorEvents } from "./observation/editor.js";

async function start(){
	var { personalChecklist, toggle } = await createScratchpad();
	var editor;

	editor = attachRTE( personalChecklist );
	await loadLines( editor );
	editorEvents( editor );
	scratchpadMouse( { personalChecklist, toggle } );
}

start();