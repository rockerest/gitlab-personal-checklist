import { persistLines } from "../common/DB.js";

export function editorEvents( editor ){
	var debounce;

	editor.on( "text-change", () => {
		clearTimeout( debounce );

		debounce = setTimeout( async () => {
			await persistLines( editor.getContents() )
		}, 900 );
	} );
}