export function getTools( node = document ){
	return {
		"qs": node.querySelector.bind( node ),
		"ce": document.createElement.bind( document )
	};
}