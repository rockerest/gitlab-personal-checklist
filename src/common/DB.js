import { uuid } from "@thomasrandolph/taproot/lib/Random.js";
import Delta from "quill-delta"
import Dexie from "dexie";

import { deltaToLines } from "./RTE.js";

function intersectLinesAndRows( lines, rows ){
	var uuids = rows.map( ( row ) => row.id );
	var uuidsCount = uuids.length;
	var linesCount = lines.length;
	var countDiff = linesCount - uuidsCount;
	var usedUuids = uuids;
	var unusedUuids = [];

	if( countDiff < 0 ){
		usedUuids = uuids.slice( 0, linesCount );
		unusedUuids = uuids.slice( linesCount - 1 );
	}
	else if( countDiff > 1 ){
		usedUuids = [ ...uuids ].concat( ...uuid( { "count": countDiff } ) );
	}
	else if( countDiff == 1 ){
		usedUuids = [ ...uuids ].concat( uuid() );
	}

	return {
		"rows": usedUuids.map( ( id ) => ( { id } ) ),
		"danglingUuids": unusedUuids,
		"uuids": usedUuids
	};
}

export async function getDatabase(){
	var db = new Dexie( "GitLabPersonalChecklist" );

	db.version( 1 ).stores( {
		"items": "&id"
	} );

	return db;
}

export async function persistLines( quillDelta ){
	var db = await getDatabase();
	var rows = await db.table( "items" ).toArray();
	var lines = deltaToLines( quillDelta );
	var { "rows": updatedRows, danglingUuids } = intersectLinesAndRows( lines, rows );

	lines.forEach( ( line, i ) => {
		updatedRows[ i ].order = i;
		updatedRows[ i ].delta = line;
	} );

	await db.table( "items" ).bulkPut( updatedRows );
	await db.table( "items" ).bulkDelete( danglingUuids );
}

export async function loadLines( editor ){
	var db = await getDatabase();
	var rows = await db.table( "items" ).toArray();

	var insertOrderLines = rows
		.sort( ( alpha, beta ) => {
			let order = 0;

			if( alpha.order > beta.order ){
				order = -1;
			}
			else if( alpha.order < beta.order ){
				order = 1;
			}

			return order;
		} )
		.map( ( line ) => line.delta );
	var firstLine = insertOrderLines.shift();
	var delta = insertOrderLines.reduce( ( d, line ) => {
		return d.compose( new Delta( line ) );
	}, new Delta( firstLine ) );

	editor.setContents( delta );
} 