# GitLab Personal Checklist


### Prepare

`npm install`  
`npm run bundle`

### Install

Right now, you'll need extensions to mess with websites' scripts and styles.  

I use [Violentmonkey](https://violentmonkey.github.io) for scripts and [Stylus](https://add0n.com/stylus.html) for styles.  

Install `./dist/GitLabPersonalChecklist.user.js` into your script extension and `./src/styles.css` into your style extension.