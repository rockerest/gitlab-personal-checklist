import { rollup } from "rollup";
import { nodeResolve as resolve } from "@rollup/plugin-node-resolve";
import cjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";

var cwd = process.cwd();
var input = {
	"plugins": [
		resolve( {
			"preferBuiltins": true
		} ),
		cjs(),
		json()
	]
};
var output = {
	"format": "iife",
	"banner": `
// ==UserScript==
// @name        GitLab Personal Checklist
// @namespace   Violentmonkey Scripts
// @match       https://gitlab.com/dashboard/todos
// @grant       none
// @version     1.0
// @author      -
// @description 3/18/2021, 5:28:40 PM
// ==/UserScript==
`
};

var modules = [
	"./src/GitLabPersonalChecklist.user.js"
];

async function bundle( path, name ){
	var compiled = await rollup( { ...input, "input": path } );

	await compiled.write( { ...output, "file": `${cwd}/dist/${name}.js` } );
}

modules.forEach( async ( path ) => {
	var parts = path.split( "/" );
	var name = parts.pop().replace( ".js", "" );

	await bundle( path, name );
} );